library(GenomicRanges)
library(rtracklayer)
library(reshape2)
library(dplyr)
library(tidyr)
library(ggplot2)

source("ori_mut_human_contexts.R")

bins_around_gr_centered=function(gr, window, n=5){
  gr_split=NULL
  
  mid=round(0.5*(start(gr)+end(gr)))
  w=window/n
  for(distance_bin in (0-n):n){
    ll=mid+w/2+(distance_bin-1)*w
    rr=mid+w/2+(distance_bin)*w
    gr_i=GRanges(seqnames(gr), IRanges( ll, end=rr ), parent=1:length(gr), bin=distance_bin) 
    if(is.null(gr_split))
      gr_split=gr_i
    else
      gr_split=c(gr_split, gr_i)
  }
  gr_split=sort(gr_split)
  gr_split
}

bins2offsets=function(bins, vwidths, mids=T){
  chBins=as.character(bins)
  sumwidths=c(0,cumsum(vwidths))
  bin2offset=c()
  for(i in 1:length(vwidths)){
    bin2offset[as.character(i)]=(sumwidths[i]+sumwidths[i+1])/2
    bin2offset[as.character(0-i)]=0-(sumwidths[i]+sumwidths[i+1])/2
  }
  bin2offset[chBins]
}

bins_around_gr_centered_varScale=function(gr, window, n=5, w0=1000, xfact=2, genome=Hsapiens, vwidths=NULL){
  gr_split=NULL
  if(!is.null(vwidths))
    n=length(vwidths)
  mid=round(0.5*(start(gr)+end(gr)))
  w=w0
  delta1=0
  delta2=0
  for(distance_bin in 1:n){
    if(!is.null(vwidths))
      w=vwidths[distance_bin]
    delta1=delta2
    delta2=delta1+w
    w=w*xfact
    
    gr_l=GRanges(seqnames(gr), IRanges( mid+delta1, end=mid+delta2 ), parent=1:length(gr), bin=distance_bin) 
    gr_r=GRanges(seqnames(gr), IRanges( mid-delta2, end=mid-delta1 ), parent=1:length(gr), bin=0-distance_bin) 
    
  
    gr_i=c(gr_l, gr_r)
    if(is.null(gr_split))
      gr_split=gr_i
    else
      gr_split=c(gr_split, gr_i)
  }
  
  seql=seqlengths(Hsapiens)
  to_rm=gr_split[start(gr_split)<0 | end(gr_split)>seql[as.character(seqnames(gr_split))],]$parent
  print("filter centers which are not within chromosome length")
  print(length(gr_split))
  gr_split=gr_split[!gr_split$parent %in% to_rm,]
  print(length(gr_split))
  gr_split=gr_split[order(gr_split$parent),]
  gr_split
}

gr_vs_gr=function(gr1, gr2, prefix1="ori", prefix2="snp"){
  gr_2_snp=findOverlaps(gr1, gr2)
  
  df1=as.data.frame(gr1[queryHits(gr_2_snp),])
  df2=as.data.frame(gr2[subjectHits(gr_2_snp),])
  gr_snp= cbind( df1,  df2  )
  
  colnames(gr_snp)=paste(c( rep(prefix1, ncol(df1)), rep(prefix2, ncol(df2)) ), colnames(gr_snp), sep=".")
  gr_snp
}

library(BSgenome.Hsapiens.UCSC.hg19)
count_nucl_content_gr=function(ori_bins_gr, window, apobec=F, exome=F){
  library(BSgenome.Hsapiens.UCSC.hg19)
  
  #ori_bins_gr
  
  exfrag=NULL
  seqfragments=getSeq(Hsapiens, ori_bins_gr)
  if(exome){
    covered=import.bed("/home/artemov/tmp/ori/polD_exome/Covered.bed")
    covered_and_bin=covered_vs_bin(covered, ori_bins_gr)
    exfrag=covered_and_bin
    seqfragments=getSeq(Hsapiens, exfrag)
    N=length(ori_bins_gr)
  }
  
  
  
  if(apobec){
    #ori_snp$ApoF=vcountPattern("TCW", seqfragments)
    #ori_snp$ApoR=vcountPattern("WGA", seqfragments)
    ori_bins_gr$ApoF=vcountPatternEx("TCA", seqfragments, exfrag, N)+vcountPatternEx("TCT", seqfragments, exfrag, N)
    ori_bins_gr$ApoR=vcountPatternEx("TGA", seqfragments, exfrag, N)+vcountPatternEx("AGA", seqfragments, exfrag, N)
    ori_bins_gr$refCount=rowSums(as.matrix(ori_bins_gr@elementMetadata[,c("ApoF", "ApoR")])*as.matrix(data.frame(ApoF=ori_bins_gr@elementMetadata$Ref=="ApoF", ApoR=ori_bins_gr@elementMetadata$Ref=="ApoR")))
  }else{
    ori_bins_gr$A=vcountPatternEx("A", seqfragments, exfrag, N)
    ori_bins_gr$C=vcountPatternEx("C", seqfragments, exfrag, N)
    ori_bins_gr$G=vcountPatternEx("G", seqfragments, exfrag, N)
    ori_bins_gr$T=vcountPatternEx("T", seqfragments, exfrag, N)
    
    #!!ori_bins_gr$refCount=rowSums(as.matrix(ori_bins_gr@elementMetadata[,c("A", "C", "G", "T")])*as.matrix(data.frame(A=ori_bins_gr$Ref=="A", C=ori_bins_gr$Ref=="C",G=ori_bins_gr$Ref=="G",T=ori_bins_gr$Ref=="T")))
  }
  
  
  ori_bins_gr
}

annotate_RefCounts=function(os, bins_gr_nuclContent){
  nuclContent=as.data.frame(bins_gr_nuclContent)[,c("parent","bin","A","C","G","T")]
  colnames(nuclContent)=paste0("ori.", colnames(nuclContent))
  ori_snp=merge(os, nuclContent, by=c("ori.parent", "ori.bin"))
  
  ori_snp$refCount=rowSums(as.matrix(ori_snp[,c("ori.A", "ori.C", "ori.G", "ori.T")])*as.matrix(data.frame(A=ori_snp$snp.Ref=="A", C=ori_snp$snp.Ref=="C",G=ori_snp$snp.Ref=="G",T=ori_snp$snp.Ref=="T")))
  ori_snp
  #os_complete=ori_snp %>% group_by(ori.parent, ori.bin) %>% summarize(N=n()) %>% complete(ori.parent, ori.bin, fill=list(N=0))
  
  #os_complete
}

complete_ori_snp_gr=function(ori_snp){
  #all_substitutions=ori_snp %>% group_by(Ref, Alt) %>% summarize()
  
  library(tidyr)
  #ori_snp2=as.data.frame(ori_snp  %>% group_by(ori.seqnames, ori.start, ori.end, ori.parent, ori.bin, snp.Ref, snp.Alt,  ori.A, ori.C, ori.G, ori.T) %>% summarize(N=n(), refCount=mean(refCount)))
  ori_snp2=as.data.frame(ori_snp  %>% group_by(ori.seqnames, ori.start, ori.end, ori.parent, ori.bin, snp.Ref, snp.Alt) %>% summarize(N=n()))
  
  
  #ori_snp2$ori_hash=as.factor(paste(ori_snp$ori.seqnames, ori_snp$ori.start, ori_snp$ori.end))
  #for(cn in c("Ref", "Alt", "distance_bin")){
  for(cn in c("ori.seqnames", "ori.start", "ori.end", "snp.Ref", "snp.Alt", "ori.bin")){
    ori_snp2[[cn]]=as.factor(ori_snp2[[cn]])
  }
  #vv FIX
  #ori_snp_complete=ori_snp2 %>% complete(nesting(ori.seqnames, ori.start, ori.end, ori.parent, ori.A, ori.C, ori.G, ori.T), nesting(snp.Ref, snp.Alt), ori.bin, fill=list(N=0, refCount=0))
  ori_snp_complete=ori_snp2 %>% complete(nesting(ori.seqnames, ori.start, ori.end, ori.parent), nesting(snp.Ref, snp.Alt), ori.bin, fill=list(N=0))
  
  ori_snp_complete$ori.start=as.integer(as.character(ori_snp_complete$ori.start))
  ori_snp_complete$ori.end=as.integer(as.character(ori_snp_complete$ori.end))
  ori_snp_complete$distance_bin=as.integer(as.character(ori_snp_complete$ori.bin))
  
  for(cn in c("ori.seqnames", "snp.Ref", "snp.Alt")){
    ori_snp_complete[[cn]]=as.character(ori_snp_complete[[cn]])
  }
  
  ori_snp_complete
}
getBinDistances=function(bins_gr){
  gr=bins_gr[bins_gr$parent==1,]
  mids=0.5*(start(gr)+end(gr))
  globmid=mean(0.5*(start(gr)+end(gr)))
  dists=mids-globmid
  data.frame(bin=sort(gr$bin), position=sort(dists), pos=as.character(sort(dists)), row.names=as.character(sort(gr$bin)))
}

main_varScale=function(l_ori_snp, window){
  lsnp=snp_vs_ori_contexts(window, filter="POLE_MSI", ori_gr = NULL, return_separately=T)
  ori_gr=lsnp$ori_gr
  #bins_gr=bins_around_gr_centered_varScale(ori_gr, w0=1000, xfact=4)
  #bins_gr=bins_around_gr_centered_varScale(ori_gr, n=50, w0=1000, xfact=1)
  #bins_gr=bins_around_gr_centered_varScale(ori_gr, vwidths = c(rep(1000,5), rep(5000,5), rep(10000,2), rep(50000,1)))
  #bins_gr=bins_around_gr_centered_varScale(ori_gr, vwidths = rep(1000,10))
  #bins_gr=bins_around_gr_centered_varScale(ori_gr, vwidths = c(rep(1000,10), rep(5000,4), rep(10000,2), rep(50000,1)))
  #!!! vwidths=c(rep(1000,10), rep(10000,4), rep(50000,1))
  #vwidths=c(rep(1000,5), rep(10000,3), rep(50000,2))
  vwidths=c(rep(1000,5), rep(20000,4))
  bins_gr=bins_around_gr_centered_varScale(ori_gr, vwidths = vwidths)
  bins_gr_nuclContent=count_nucl_content_gr(bins_gr)
  
  os_varScale=gr_vs_gr(bins_gr_nuclContent, lsnp$snp_gr)
  #os_varScale=annotate_RefCounts(os_varScale)
  os_complete=complete_ori_snp_gr(os_varScale)
  os_complete_ann=annotate_RefCounts(os_complete, bins_gr_nuclContent)
  
  os_complete_agg=os_complete_ann %>% group_by(ori.bin, snp.Ref, snp.Alt) %>% summarize(N=sum(N), refCount=sum(refCount)) %>% mutate(rate=N/refCount)
  bin2dist=getBinDistances(bins_gr)
  os_complete_agg$bin=factor( bin2dist[as.character(os_complete_agg$ori.bin),]$pos, levels=bin2dist$pos )
  #ggplot(os_complete_agg, aes(y=rate, x=ori.bin))+geom_bar(stat="identity")+facet_grid(snp.Ref~snp.Alt)
  
  os_complete_agg$offset= bins2offsets(os_complete_agg$ori.bin, vwidths)
  
  os_complete_agg$sd=sqrt(os_complete_agg$N)/os_complete_agg$refCount
  ori_snp_agg_norm=os_complete_agg %>% group_by(snp.Ref, snp.Alt) %>% mutate(normrate=rate/mean(rate[ori.bin=="-5" | ori.bin=="5"]), normsd=sd/mean(rate[ori.bin=="-5" | ori.bin=="5"]))
  
  ggplot(os_complete_agg, aes(y=rate, x=offset))+geom_line()+facet_grid(snp.Ref~snp.Alt)+ theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=.5))
  ggplot(ori_snp_agg_norm, aes(y=normrate, x=offset))+geom_line()+facet_grid(snp.Ref~snp.Alt)+ theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=.5))
  
  #ggplot(os_complete_agg, aes(y=rate, x=bin))+geom_bar(stat="identity")+facet_grid(snp.Ref~snp.Alt)+ theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=.5))
  
  #os_complete = os_varScale %>% group_by(o.parent, o.bin, dataset) %>% summarize(N=sum(s.mcols)) %>% complete(o.parent, o.bin, dataset, fill=list(N=0))
  #os_agg=os_complete %>% group_by(o.bin, dataset) %>% summarize(N=sum(N))
  #os_complete = os_varScale %>% group_by(ori.parent, ori.bin) %>% summarize(N=n()) %>% complete(ori.parent, ori.bin, fill=list(N=0))
  #os_agg=os_complete %>% group_by(ori.bin) %>% summarize(N=sum(N))
  
  #ggplot(os_agg, aes(x=ori.bin, y=N))+geom_line(stat="identity")#+facet_grid(.~dataset)
  #ggsave("profile_ori_varScale.pdf", width = 20, height=7)
  #ggsave("profile_ori_varScale_1024K.pdf", width = 7, height=7)
  ggsave("profile_ori_varScale_21K.pdf", width = 7, height=7)
  
  
  
  #ori_snp_MSI=count_nucl_content(complete_ori_snp(os_varScale), window, apobec=F)
  #obs_exp_mutability(ori_snp_MSI)
}

